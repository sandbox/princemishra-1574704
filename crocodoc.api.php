<?php


/**
 * implements hook_crocodoc_settings_alter
 * Allows finer control of crocodoc settings
 */
function hook_crocodoc_settings_alter(&$settings, $context) {
  // enabled annotations where user has edit permission on the node

  if ($context['entity_type'] == 'node' && node_access('update', $context['entity'])) {
    $settings['allow_annotations'] = 1;
  }
}
