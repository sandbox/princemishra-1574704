<?php

/**
* implements hook_field_formatter_info
**/
function crocodoc_field_formatter_info() {
  // @TODO: Provide other formatters such as modal, thumbnail or link etc
  return array(
    'crocodoc_iframe' => array(
      'label' => t('Crocodoc IFrame'),
      'field types' => array('file', 'text'),
      'settings' => array(
        'frame_width' => '100%',
        'frame_height' => '800px',
        'allow_annotations' => 1,
        'discard_annotations' => 0,
        'downloadable' => 1,
        'copy_protected' => 0,
        'sidebar' => 'auto',
//        'copy_text' => ''
      ),
    ),
  );
}

/**
* implements hook_field_formatter_settings_summary
**/
function crocodoc_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary_text = '';

  if ($display['type'] == 'crocodoc_iframe' ) {
    $summary = array();
    $summary[] = $settings['frame_width'] . ' X ' . $settings['frame_height'];
    if ( $settings['allow_annotations'] ) {
      $annotations = t('Annotations');
      if ( $settings['discard_annotations'] )
        $annotations .= t(' discarded');
      $summary[] = $annotations;
    }
    if ( $settings['downloadable'] )
      $summary[] = t('Downloadable');
    if ( $settings['copy_protected'] )
      $summary[] = t('Copy Protected');
/*
    if ( !empty($settings['copy_text']) )
      $summary[] = t('Copy text into %field', array('%field' => $settings['copy_text']));
*/

    $summary_text = '<ul><li>' . implode('</li><li>', $summary) . '</li></ul>';
  }

  return $summary_text;
}

/**
* implements hook_field_formatter_settings_form
**/
function crocodoc_field_formatter_settings_form($field, $instance, $view_mode, &$form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  if ($display['type'] == 'crocodoc_iframe' ) {
    $element['frame_width'] = array(
      '#title' => t('Frame Width'),
      '#type' => 'textfield',
      '#default_value' => $settings['frame_width'],
      '#description' => t('Set the width of the Iframe and units (eg 100% or 300px).'),
      '#size' => 4,
    );
    $element['frame_height'] = array(
      '#title' => t('Frame Height'),
      '#type' => 'textfield',
      '#default_value' => $settings['frame_height'],
      '#description' => t('Set the height of the Iframe and units (eg 100% or 300px).'),
      '#size' => 4,
    );
    $element['allow_annotations'] = array(
      '#title' => t('Allow Annotations'),
      '#type' => 'checkbox',
      '#default_value' => $settings['allow_annotations'],
      '#description' => t('All users with view permissions to this field will be able to add their own annotations.'),
    );
    $element['discard_annotations'] = array(
      '#title' => t('Discard Annotations'),
      '#type' => 'checkbox',
      '#default_value' => $settings['discard_annotations'],
      '#description' => t('Prevents document changes such as creating, editing, or deleting annotations from being persisted.'),
    );
    $element['downloadable'] = array(
      '#title' => t('Downloadable'),
      '#type' => 'checkbox',
      '#default_value' => $settings['downloadable'],
      '#description' => t('Allow viewers to download the original document.'),
    );
    $element['copy_protected'] = array(
      '#title' => t('Prevent copying text'),
      '#type' => 'checkbox',
      '#default_value' => $settings['copy_protected'],
      '#description' => t("Prevents document text selection. Although copying text will still be technically possible since it's just HTML, enabling this option makes doing so difficult."),
    );
    $element['sidebar'] = array(
      '#title' => t('Sidebar'),
      '#type' => 'select',
      '#default_value' => $settings['sidebar'],
      '#description' => t("Sets whether the viewer sidebar (used for listing annotations) is included."),
      '#options' => array(
        'none' => t('No Sidebar'),
        'collapse' => t('Collapsed Sidebar'),
        'visible' => t('Open Sidebar'),
        'auto' => t('Auto - open if annotations exist'),
      ),
    );
/*
    $element['copy_text'] = array(
      '#title' => t('Copy text into'),
      '#type' => 'textfield',
      '#default_value' => $settings['copy_text'],
      '#description' => t('Optionally have the extracted text of the document copied into another field in this node. Please enter the field name such as "field_document_text". (todo: make part of file entity and/or select list)'),
    );
*/
  }


  return $element;
}

/*
 * Implements hook_form_alter
 */
function crocodoc_form_alter($form, &$form_state) {
  if ( $form['#form_id'] == 'field_ui_display_overview_form' ) {
    $form['#submit'][] = 'crocodoc_field_formatter_settings_form_submit';
  }
}

/* Submission of settings */
function crocodoc_field_formatter_settings_form_submit($form, &$form_state) {
  // Clear the cache!
  cache_clear_all('cuuid:*', 'cache', true);
}

/**
* implements hook_field_formatter_view
**/
function crocodoc_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'crocodoc_iframe':
      foreach ($items as $delta => $item) {

        // allow other modules to alter the settingss
        $settings = $display['settings'];
        $context = array('entity_type' => $entity_type, 'entity' => $entity, 'field' => $field);
        drupal_alter('crocodoc_settings_alter', $settings, $context);

        $element[$delta] = array(
          '#theme' => 'crocodoc_iframe_display',
          '#item' => (object) $item,
          '#settings' => $settings,
          '#entity' => $entity,
          '#field' => $field,
        );
      }
      break;
  }
  return $element;
}

/**
* implements hook_theme
**/
function crocodoc_theme() {
  return array(
    'crocodoc_iframe_display' => array(
      'variables' => array(
        'item' => NULL,
        'settings' => NULL,
        'entity' => NULL,
        'field' => NULL,
      ),
    ),
  );
}


function theme_crocodoc_iframe_display($variables) {
  $crocodoc_uuid = NULL;

  $item = $variables['item'];
  $field = $variables['field'];
  if ($field['type'] == 'file') {
    if (isset($item->crocodoc_uuid)) {
      $crocodoc_uuid = $item->crocodoc_uuid;
    }
    else {
      $crocodoc_uuid = crocodoc_push_document($item, $variables['entity']->nid);
    }
  }
  else if ($field['type'] == 'text') {
    $crocodoc_uuid = $item->value;
  }

  $settings = $variables['settings'];
  $settings['is_admin'] = node_access('update', $variables['entity']);

  if ($crocodoc_uuid != NULL && $crocodoc_uuid != false ) {
    if ($session_key = crocodoc_get_file_session_key($crocodoc_uuid, $settings)) {
      return '<iframe src="https://crocodoc.com/view/' . $session_key . '" width="' . $settings['frame_width'] . '" height="' . $settings['frame_height'] . '"></iframe>';
    }
  }

  return '<div class="crocodoc-fail">' . t('Unable to load document') . '</div>';
}

