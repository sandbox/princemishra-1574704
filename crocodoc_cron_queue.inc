<?php

/**
* Implements hook_cron_queue_info().
*/
function crocodoc_cron_queue_info() {
  $queues = array();
  $queues['crocodoc_queue'] = array(
    'worker callback' => 'crocodoc_process_queue_item', //function to call for each item
    'time' => 120, //seconds to spend working on the queue
  );

  return $queues;
}

function crocodoc_process_queue_item($queue_item) {
  switch($queue_item['task']) {
    case 'push':
      crocodoc_push_document($queue_item['file']);
    break;
    case 'delete':
      $uuid = $queue_item['file']->crocodoc_uuid;
      crocodoc_delete_document($uuid);
    break;
    case 'delete_by_uuid':
      crocodoc_delete_document($queue_item['uuid']);
    break;
  }
}
